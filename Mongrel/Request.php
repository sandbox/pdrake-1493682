<?php

namespace Mongrel;

class Request extends \Symfony\Component\HttpFoundation\Request {
  protected $sender;
  protected $path;
  protected $conn_id;
  protected $request_headers;
  protected $body;
  protected $data;

  static public function parse($msg) {
    list(
      $sender,
      $conn_id,
      $path,
      $rest
    ) = explode(' ', $msg, 4);
    $hd = Tool::parse_netstring($rest);
    $headers = $hd[0];
    $rest = $hd[1];
    $hd = Tool::parse_netstring($rest);
    $body = $hd[0];

    $headers = json_decode($headers);

    $cookies = array();
    if (!empty($headers->Cookie)) {
      $cookies = $headers->Cookie;
    }

    $ip = 'x-forwarded-for';

    $request = Request::create($path, $headers->METHOD, array(), $cookies, array(), array('HTTP_HOST' => $headers->host, 'REMOTE_ADDR' => $headers->$ip, 'SERVER_SOFTWARE' => 'Mongrel2'), null);
    $request->overrideGlobals();
    $request->build($sender, $conn_id, $path, $headers, $body);
    return $request;
  }

  function build($sender, $conn_id, $path, $body) {
    $this->sender = $sender;
    $this->path = $path;
    $this->conn_id = $conn_id;
    $this->body = $body;

    if ($this->getMethod() == 'JSON') {
      $this->data = json_decode($body);
    } else {
      $this->data = array();
    }
  }

  public function getSender() {
    return $this->sender;
  }

  public function getConnectionId() {
    return $this->conn_id;
  }

  public function is_disconnect() {
    if ($this->getMethod() == 'JSON') {
      //return $this->data['type'] == 'disconnect';
    }
  }
}
