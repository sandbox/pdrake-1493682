<?php

use Drupal\Core\DrupalKernel;
use Mongrel\Connection;

/**
 * Root directory of Drupal installation.
 */
define('DRUPAL_ROOT', getcwd());
define('M2DRUPAL_DIR', '/sites/all/modules/contrib/m2drupal');

// Configure server superglobal
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['SERVER_SOFTWARE'] = 'Mongrel2';

// Bootstrap the lowest level of what we need.
require_once DRUPAL_ROOT . '/core/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_VARIABLES);

// Require needed files
require_once DRUPAL_ROOT . '/' . variable_get('session_inc', 'core/includes/session.inc');
require_once DRUPAL_ROOT . '/core/includes/common.inc';

// Include and activate the class loader.
$loader = drupal_classloader();

// Register Mongrel2 namespace.
$loader->registerNamespaces(
  array(
    'Mongrel' => DRUPAL_ROOT . M2DRUPAL_DIR,
  )
);

$sender_id = "82209006-86FF-4982-B5EA-D1E29E55D481";
$conn = new Connection($sender_id, "tcp://127.0.0.1:9997", "tcp://127.0.0.1:9996");

while (true) {
  $request = $conn->recv();

  $_SERVER['REMOTE_ADDR'] = $request->getClientIp();
  $_SERVER['REQUEST_METHOD'] = $request->getMethod();

  if ($request->is_disconnect()) {
    continue;
  }

  // Bootstrap DRUPAL_BOOTSTRAP_FULL without repeating N < DRUPAL_BOOTSTRAP_VARIABLES
  global $base_url;
  unset($base_url);
  drupal_session_initialize();
  _drupal_bootstrap_page_header();
  drupal_language_initialize();
  _drupal_bootstrap_full();

  // Bootstrap DRUPAL_BOOTSTRAP_FULL parts that won't happen in _drupal_bootstrap_full(); due to static variable
  // Initialize $_GET['q'] prior to invoking hook_init().
  drupal_path_initialize();

  // Let all modules take action before the menu system handles the request.
  // We do not want this while running update.php.
  if (!defined('MAINTENANCE_MODE') || MAINTENANCE_MODE != 'update') {
    // Prior to invoking hook_init(), initialize the theme (potentially a custom
    // one for this page), so that:
    // - Modules with hook_init() implementations that call theme() or
    //   theme_get_registry() don't initialize the incorrect theme.
    // - The theme can have hook_*_alter() implementations affect page building
    //   (e.g., hook_form_alter(), hook_node_view_alter(), hook_page_alter()),
    //   ahead of when rendering starts.
    menu_set_custom_theme();
    drupal_theme_initialize();
    module_invoke_all('init');
  }

  $kernel = new DrupalKernel();
  $response = $kernel->handle($request);

  $conn->reply_http($request, $response->getContent(), $response->getStatusCode(), $response::$statusTexts[$response->getStatusCode()], $response->headers->all());
}
