# m2drupal

## Usage
  - Configure Mongrel2 as a web server (drupal.conf provides a sample configuration)
  - Copy the m2drupal module to sites/all/modules/contrib/m2drupal
  - Copy m2drupal.php to your drupal root directory
  - Edit m2drupal.php to changed M2DRUPAL_DIR if you placed the module in a different directory
  - Run m2drupal.php using your choice of process manager